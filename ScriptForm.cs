﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RDP_script
{
    public partial class ScriptForm : Form
    {
        public ScriptForm()
        {
            InitializeComponent();

            JsonParse();
        }

        private void closeButton_Click(object sender, EventArgs e) //кнопка закрывания программы
        {
            this.Hide();
            foreach (var item in ProcessService.ListRdpsConnection)
            {
                item.Value.KillRdpConnection();
            }
            foreach (var item in ProcessService.ListVpnsConnection)
            {
                item.Value.KillVpn();
            }
            VpnService.KillRegistryVpn();
            Application.Exit();
        }

        Point lastPoint;
        private void mainPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void mainPanel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void closeButton_MouseHover(object sender, EventArgs e)
        {
            closeButton.BackColor = Color.White;
        }//кнопка закрывания программы

        private void closeButton_MouseLeave(object sender, EventArgs e)
        {
            closeButton.BackColor = Color.DarkGray;
        }

        private void buttonRdp_Click(object sender, EventArgs e)
        {
            ProcessService.ListRdpsConnection[comboBoxRdps.Text].ConnectToRdp();
        }

        private void buttonVpnConnect_Click(object sender, EventArgs e)
        {
            VpnService.KillRegistryVpn();
            labelVpnStatus.Text = "Идет подключение...";
            ProcessService.ListVpnsConnection[comboBoxVpns.Text].ConnectToVpn();
            labelVpnStatus.Text = "Вы подлкючились к VPN";
            
          }
        private void buttonVpnDisconnect_Click(object sender, EventArgs e)
        {
            ProcessService.ListVpnsConnection[comboBoxVpns.Text].KillVpn();
            VpnService.KillRegistryVpn();
            labelVpnStatus.Text = "Вы отключились от VPN";
        }
        private void JsonParse()
        {
            dynamic jsonParse = JsonConvert.DeserializeObject<Root>(RestConnectToApi.jsonData);

            for (int i = 0; i < jsonParse.RetServers.Length; i++)
            {
                var source = jsonParse.RetServers[i];
                ProcessService.ListServersConnection.Add($"{source.id}", $"{source.host}");
            }

            for (int i = 0; i < jsonParse.RetRdps.Length; i++)
            {
                var source = jsonParse.RetRdps[i];
                string hostRdp = ProcessService.ListServersConnection[source.relationsServer];
                ProcessService.ListRdpsConnection.Add($"{hostRdp}, {source.username}, {source.password}", new RdpService(hostRdp, source.username, source.password));
            }
            comboBoxRdps.DataSource = new BindingSource(ProcessService.ListRdpsConnection, null);
            comboBoxRdps.DisplayMember = "Key";
            comboBoxRdps.ValueMember = "Value";

            for (int i = 0; i < jsonParse.RetVpns.Length; i++)
            {
                var source = jsonParse.RetVpns[i];
                
                for (int j = 0; j < source.RetProto.Length; j++)
                {
                    var sourceProto = source.RetProto[j];
                    if(sourceProto.component == "vpn-proto.l2tp")
                    {
                        ProcessService.ListVpnsConnection.Add($"{source.hostVpn}, {sourceProto.username}, {sourceProto.password}, {sourceProto.ipsec}", new VpnService(source.hostVpn, source.hostVpn, sourceProto.username, sourceProto.password, sourceProto.ipsec ));
                    }
                    ProcessService.ListVpnsConnection.Add($"{source.hostVpn}, {sourceProto.username}, {sourceProto.password}", new VpnService(source.hostVpn, source.hostVpn, sourceProto.username, sourceProto.password));

                }
            }
            comboBoxVpns.DataSource = new BindingSource(ProcessService.ListVpnsConnection, null);
            comboBoxVpns.DisplayMember = "Key";
            comboBoxVpns.ValueMember = "Value";
        }

       
    }

    public class ProcessService
    {
        public static Dictionary<string, RdpService> ListRdpsConnection = new Dictionary<string, RdpService>();
        public static Dictionary<string, VpnService> ListVpnsConnection = new Dictionary<string, VpnService>();
        public static Dictionary<string, string> ListServersConnection = new Dictionary<string, string>();

        protected enum ProcessType
        {
            cmd,
            pw //powershell
        };

        protected void ProcessStart(ProcessType type, string command, bool waitForExit, bool threadSleep, int timesSleep = 0)
        {
            if (threadSleep) Thread.Sleep(timesSleep);
            Process process = Process.Start(new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                Verb = "runas",
                FileName = type == ProcessType.cmd ? @"cmd" : @"C:\Windows\SysWOW64\WindowsPowerShell\v1.0\powershell.exe",
                Arguments = type == ProcessType.cmd ? $"/c {command}" : $"/command {command}",
                

            });
            if (waitForExit)
            {
                process.WaitForExit();
            }

            
        }
    }
    public class RdpService : ProcessService
    {
        public string serverAddress;
        public string username;
        public string password;
        
        public RdpService(string serverAddress, string username, string password) { this.serverAddress = serverAddress; this.username = username; this.password = password; }

        public void ConnectToRdp()
        {
            string curCommand = $"cmdkey /generic:{serverAddress} /user:{username} /password:{password} \n mstsc /v:{serverAddress} | out-null \n cmdkey /delete:{serverAddress} | out-null";

            ProcessStart(ProcessType.pw, curCommand, true, false);
            KillRdpConnection();
        }

        public void KillRdpConnection()
        {
            string curCommand = $"cmdkey /delete:{serverAddress}";

            ProcessStart(ProcessType.pw, curCommand, true, false);

            KillRegistryRdp();
        }
        private void KillRegistryRdp()
        {
            string pathRegistry = @"Software\Microsoft\Terminal Server Client\Default";
            RegistryKey key = Registry.CurrentUser.CreateSubKey(pathRegistry);
            
            foreach (var k in key.GetValueNames())
            {
                if (k.StartsWith("MR"))
                {
                    string currentNameKey = (string)key.GetValue(k);
                    string command = $"cmdkey /delete:TERMSRV/{currentNameKey}";
                    
                    ProcessStart(ProcessType.cmd, command, true, false);
                }
            }
            AfterSaveRdp();
        }
 
        private void AfterSaveRdp()
        {
            string command = "Get-ChildItem 'HKCU:\\Software\\Microsoft\\Terminal Server Client' -Recurse | Remove-ItemProperty -Name UsernameHint -Ea 0\n" +
                "Remove-Item -Path 'HKCU:\\Software\\Microsoft\\Terminal Server Client\\servers' -Recurse  2>&1 | Out-Null\n" +
                "Remove-ItemProperty -Path 'HKCU:\\Software\\Microsoft\\Terminal Server Client\\Default' 'MR*'  2>&1 | Out-Null\n" +
                "$docsfoldes = [environment]::getfolderpath('mydocuments') + '\\Default.rdp';remove-item  $docsfoldes  -Force  2>&1 | Out-Null\n";
            ProcessStart(ProcessType.pw, command, true, false);
        }
    }
    public class VpnService : ProcessService
    {
        private string nameVpn;
        private string username;
        private string serverAddress;
        private string password;
        private string ipsec;

        public VpnService(string nameVpn, string serverAddress, string username, string password) { this.nameVpn = nameVpn; this.serverAddress = serverAddress; this.username = username; this.password = password; }

        public VpnService(string nameVpn, string serverAddress, string username, string password, string ipsec) { this.nameVpn = nameVpn; this.serverAddress = serverAddress; this.username = username; this.password = password; this.ipsec = ipsec; }

        public void ConnectToVpn()
        {
            AddVpn();
            string curCommand = $"rasdial {nameVpn} {username} {password}";

            ProcessStart(ProcessType.pw,curCommand, true, false);
        }

        private void AddVpn()
        {
            string curCommand = ipsec == null ? $"Add-VpnConnection -Name \"{nameVpn}\" -ServerAddress \"{serverAddress}\" -TunnelType \"Pptp\" -EncryptionLevel \"Required\" -AuthenticationMethod MSChapv2" : $"Add-VpnConnection -Name \"{nameVpn}\" -ServerAddress {serverAddress} -TunnelType \"L2tp\" -L2tpPsk \"{ipsec}\";\n";
            
            ProcessStart(ProcessType.pw, curCommand, true, false);
        }

        public void KillVpn()
        {
            string curCommand = $"rasdial {nameVpn} /disconnect\n" + $"Remove-VpnConnection -Name \"{nameVpn}\" -Force -PassThru";

            ProcessStart(ProcessType.pw, curCommand, true, false);

        }

        public static void KillRegistryVpn()
        {
            string pathRegistry = @"Software\Microsoft\Windows\CurrentVersion\Internet Settings\";
            RegistryKey key = Registry.CurrentUser.CreateSubKey(pathRegistry);

            key.DeleteSubKey("Connections");
        }
    }

    //Json deserialize
    public class Rdps
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("username")]
        public string username { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("relationsServer")]
        public string relationsServer { get; set; }

        [JsonProperty("relationVpn")]
        public string relationsVpn { get; set; }

    }
    public class Proto
    {
        [JsonProperty("__component")]
        public string component { get; set; }
        
        [JsonProperty("_id")]
        public string id { get; set; }
        [JsonProperty("ipsec")]
        public string ipsec { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }
        
        [JsonProperty("username")]
        public string username { get; set; }
        
    }
    public class Vpns
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("hostVpn")]
        public string hostVpn { get; set; }

        [JsonProperty("proto")]
        public Proto[] RetProto { get; set; }

    }
    public class Servers
    {
        [JsonProperty("id")]
        public string id { get; set; }

        [JsonProperty("host")]
        public string host { get; set; }

        [JsonProperty("name")]
        public string name { get; set; }

    }
    public class Root
    {
        [JsonProperty("confirmed")]
        public string confirmed { get; set; }
        
        [JsonProperty("blocked")]
        public string blocked { get; set; }

        [JsonProperty("rdps")]
        public Rdps[] RetRdps { get; set; }
        
        [JsonProperty("vpns")]
        public Vpns[] RetVpns { get; set; }
        
        [JsonProperty("servers")]
        public Servers[] RetServers { get; set; }

    }
    ////
}
