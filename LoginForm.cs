﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RDP_script
{
    public partial class LoginForm : Form
    {
        private Point last_point;

        public LoginForm()
        {
            InitializeComponent();

            this.passField.AutoSize = false;
            this.passField.Size = new Size(this.passField.Size.Width, 44);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void closeButton_MouseEnter(object sender, EventArgs e)
        {
            closeButton.ForeColor = Color.White;
        }

        private void closeButton_MouseLeave(object sender, EventArgs e)
        {
            closeButton.ForeColor = Color.DarkGray;
        }

        private void mainPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                this.Left += e.X - last_point.X;
                this.Top += e.Y - last_point.Y;
            }
        }

        private void mainPanel_MouseDown(object sender, MouseEventArgs e)
        {
            last_point = new Point(e.X, e.Y);
            MessageBox.Show(Convert.ToString(last_point));
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            
            RestConnectToApi obj = new RestConnectToApi("http://10.6.1.196:1337/def/authentication", "post", loginField.Text, passField.Text);
            
            if(obj.ConnectToApi())
            {
                this.Hide();
                ScriptForm scriptForm = new ScriptForm() {Owner = this};
                scriptForm.Show();
            }
            else
                MessageBox.Show("Not correct password or login!");
            
        }
        private void loginField_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
                buttonLogin_Click(buttonLogin, null);
        }

        private void passField_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyValue == (char)Keys.Enter)
                buttonLogin_Click(buttonLogin, null);
        }
    }
    public class RestConnectToApi
    {
        public static string jsonData;
        private string url;
        private string nameMethod;
        private string username;
        private string password;

        public RestConnectToApi(string url, string nameMethod, string username, string password ) { this.url = url; this.nameMethod = nameMethod.ToLower(); this.username = username; this.password = password; }

        public bool ConnectToApi()
        {
            try
            {
                RestClient client = new RestClient(url);
                RestRequest request = new RestRequest(nameMethod == "get" ? Method.GET : Method.POST);

                request.AddJsonBody(RetBodyJson());

                IRestResponse response = client.Execute(request);

                if (!response.IsSuccessful) return false;

                string stream = response.Content;
                //jsonData = JsonConvert.DeserializeObject(stream);
                jsonData = stream;
                MessageBox.Show(stream);
                Console.WriteLine(jsonData);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            
        }

        private string RetBodyJson()
        {
            return $"{{ \"identifier\": \"{username}\", \"password\": \"{password}\"}}";
        }


        
    }
}
