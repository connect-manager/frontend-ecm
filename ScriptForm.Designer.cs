﻿
namespace RDP_script
{
    partial class ScriptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeButton = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.buttonVpnDisconnect = new System.Windows.Forms.Button();
            this.buttonVpnConnect = new System.Windows.Forms.Button();
            this.buttonRdp = new System.Windows.Forms.Button();
            this.comboBoxVpns = new System.Windows.Forms.ComboBox();
            this.labelVpn = new System.Windows.Forms.Label();
            this.comboBoxRdps = new System.Windows.Forms.ComboBox();
            this.labelRdp = new System.Windows.Forms.Label();
            this.labelVpnStatus = new System.Windows.Forms.Label();
            this.mainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // closeButton
            // 
            this.closeButton.AutoSize = true;
            this.closeButton.BackColor = System.Drawing.Color.DarkGray;
            this.closeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.closeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closeButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeButton.Location = new System.Drawing.Point(455, 9);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(23, 21);
            this.closeButton.TabIndex = 1;
            this.closeButton.Text = "X";
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            this.closeButton.MouseLeave += new System.EventHandler(this.closeButton_MouseLeave);
            this.closeButton.MouseHover += new System.EventHandler(this.closeButton_MouseHover);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.mainPanel.BackgroundImage = global::RDP_script.Properties.Resources.background;
            this.mainPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainPanel.Controls.Add(this.labelVpnStatus);
            this.mainPanel.Controls.Add(this.buttonVpnDisconnect);
            this.mainPanel.Controls.Add(this.buttonVpnConnect);
            this.mainPanel.Controls.Add(this.buttonRdp);
            this.mainPanel.Controls.Add(this.comboBoxVpns);
            this.mainPanel.Controls.Add(this.labelVpn);
            this.mainPanel.Controls.Add(this.comboBoxRdps);
            this.mainPanel.Controls.Add(this.labelRdp);
            this.mainPanel.Controls.Add(this.closeButton);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(490, 442);
            this.mainPanel.TabIndex = 1;
            this.mainPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mainPanel_MouseDown);
            this.mainPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mainPanel_MouseMove);
            // 
            // buttonVpnDisconnect
            // 
            this.buttonVpnDisconnect.Location = new System.Drawing.Point(355, 145);
            this.buttonVpnDisconnect.Name = "buttonVpnDisconnect";
            this.buttonVpnDisconnect.Size = new System.Drawing.Size(101, 21);
            this.buttonVpnDisconnect.TabIndex = 8;
            this.buttonVpnDisconnect.Text = "Disconnect VPN";
            this.buttonVpnDisconnect.UseVisualStyleBackColor = true;
            this.buttonVpnDisconnect.Click += new System.EventHandler(this.buttonVpnDisconnect_Click);
            // 
            // buttonVpnConnect
            // 
            this.buttonVpnConnect.Location = new System.Drawing.Point(355, 118);
            this.buttonVpnConnect.Name = "buttonVpnConnect";
            this.buttonVpnConnect.Size = new System.Drawing.Size(101, 21);
            this.buttonVpnConnect.TabIndex = 7;
            this.buttonVpnConnect.Text = "Connect to VPN";
            this.buttonVpnConnect.UseVisualStyleBackColor = true;
            this.buttonVpnConnect.Click += new System.EventHandler(this.buttonVpnConnect_Click);
            // 
            // buttonRdp
            // 
            this.buttonRdp.Location = new System.Drawing.Point(355, 51);
            this.buttonRdp.Name = "buttonRdp";
            this.buttonRdp.Size = new System.Drawing.Size(101, 21);
            this.buttonRdp.TabIndex = 6;
            this.buttonRdp.Text = "Connect to RDP";
            this.buttonRdp.UseVisualStyleBackColor = true;
            this.buttonRdp.Click += new System.EventHandler(this.buttonRdp_Click);
            // 
            // comboBoxVpns
            // 
            this.comboBoxVpns.FormattingEnabled = true;
            this.comboBoxVpns.Location = new System.Drawing.Point(12, 118);
            this.comboBoxVpns.Name = "comboBoxVpns";
            this.comboBoxVpns.Size = new System.Drawing.Size(314, 21);
            this.comboBoxVpns.TabIndex = 5;
            // 
            // labelVpn
            // 
            this.labelVpn.AutoSize = true;
            this.labelVpn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVpn.Location = new System.Drawing.Point(12, 93);
            this.labelVpn.Name = "labelVpn";
            this.labelVpn.Size = new System.Drawing.Size(47, 17);
            this.labelVpn.TabIndex = 4;
            this.labelVpn.Text = "VPNs:";
            // 
            // comboBoxRdps
            // 
            this.comboBoxRdps.FormattingEnabled = true;
            this.comboBoxRdps.Location = new System.Drawing.Point(12, 51);
            this.comboBoxRdps.Name = "comboBoxRdps";
            this.comboBoxRdps.Size = new System.Drawing.Size(314, 21);
            this.comboBoxRdps.TabIndex = 3;
            // 
            // labelRdp
            // 
            this.labelRdp.AutoSize = true;
            this.labelRdp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRdp.Location = new System.Drawing.Point(12, 26);
            this.labelRdp.Name = "labelRdp";
            this.labelRdp.Size = new System.Drawing.Size(48, 17);
            this.labelRdp.TabIndex = 2;
            this.labelRdp.Text = "RDPs:";
            // 
            // labelVpnStatus
            // 
            this.labelVpnStatus.AutoSize = true;
            this.labelVpnStatus.Location = new System.Drawing.Point(76, 99);
            this.labelVpnStatus.Name = "labelVpnStatus";
            this.labelVpnStatus.Size = new System.Drawing.Size(0, 13);
            this.labelVpnStatus.TabIndex = 9;
            // 
            // ScriptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 442);
            this.Controls.Add(this.mainPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScriptForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Script";
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label closeButton;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button buttonVpnConnect;
        private System.Windows.Forms.Button buttonRdp;
        private System.Windows.Forms.ComboBox comboBoxVpns;
        private System.Windows.Forms.Label labelVpn;
        private System.Windows.Forms.ComboBox comboBoxRdps;
        private System.Windows.Forms.Label labelRdp;
        private System.Windows.Forms.Button buttonVpnDisconnect;
        private System.Windows.Forms.Label labelVpnStatus;
    }
}